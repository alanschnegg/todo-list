# Todo List

## Installation et Mise en Route

```
https://gitlab.com/alanschnegg/todo-list
cd todo-list
system start mysql
php artisan serve --host=127.0.0.1 --port=8000
```

## Utilisation

Il vous reste maintenant plus qu'à aller sur [localhost:8000/](localhost:8000/) et vous connecter en utilisant un utilisateur déjà rentrer ou en créant un nouveau.