new Vue({
    el: '#app',

    data: {
        connection: true,
        inscription: false,
        passwordFieldType: 'password',
        eyes: '../img/index/eyes_open.png',
        email: '',
        password: '',
        firstname: '',
        lastname: '',
        dataUser: [{name: "Alan", lastname: "Schnegg", email:"alan.schnegg@epitech.eu", password:"admin"}],
    },

    methods: {
        switch_inscription: function () {
            this.connection = false;
            this.inscription = true;
            this.passwordFieldType = 'password';
            this.eyes = '../img/index/eyes_open.png';
            this.password = '';
            this.email = '';
            this.firstname = '';
            this.lastname = '';
        },

        switch_connexion: function () {
            this.connection = true;
            this.inscription = false;
            this.passwordFieldType = 'password';
            this.eyes = '../img/index/eyes_open.png';
            this.password = '';
            this.email = '';
            this.firstname = '';
            this.lastname = '';
        },

        showPassword: function() {
            this.passwordFieldType = this.passwordFieldType === 'password' ? 'text' : 'password',
            this.eyes = this.passwordFieldType === 'password' ? '../img/index/eyes_open.png' : '../img/index/eyes_close.png'
        },

        test: function () {
            axios.post('/todolist', {
                email: 'root@root',
                password: 'toto'
            })
                .then(function (response) {
                    console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    }
})
