new Vue({
    el: '#app',

    data: {
        list: [{name: 'Secure', task: []}, {name: 'Course', task: ['Lait', 'Beure']}, {name: 'Travail', task: ['crud', 'list', 'task']}],
        done: [[], [true, false], [false, true, true]],
        newListName: '',
        newTaskName: '',
        buttonTask: false,
        cross: '../img/todolist/cross.png',
    },

    mounted: function () {
        this.createList(),
        this.createTask(0)
    },

    methods: {
        createList: function() {
            var vm = this;

            var table = document.createElement("table");

            var tr = table.insertRow(-1);
            var th = document.createElement("th");
            th.innerHTML = "Liste de tâche";
            tr.appendChild(th);


            for (var i = 1; i < this.list.length; i++) {
                tr = table.insertRow(-1);

                var tabCell = tr.insertCell(-1);
                tabCell.innerHTML = this.list[i].name;
                tabCell.setAttribute("id", i);
                tabCell.onclick = function() { vm.createTask(this.id)};
            }

            var divContainer = document.getElementById("showList");
            divContainer.innerHTML = "";
            divContainer.appendChild(table);
        },

        createTask: function(id) {
            var vm = this;

            var delFocus = document.getElementsByClassName("focus");
            if (delFocus[0])
                delFocus[0].setAttribute("class", "");
            var setFocus = document.getElementById(id);
            if (setFocus)
                setFocus.setAttribute("class", "focus");

            var table = document.createElement("table");
            table.setAttribute("class", "tableTask");
            table.setAttribute("id", id);

            var tr = table.insertRow(-1);
            var th = document.createElement("th");
            th.innerHTML = "Tâche";
            tr.appendChild(th);


            for (var i = 0; i < this.list[id].task.length; i++) {
                tr = table.insertRow(-1);

                var tabCell = document.createElement("td");

                tabCell.innerHTML = this.list[id].task[i];
                tabCell.setAttribute("id", i);
                var img = document.createElement("img");
                img.setAttribute("src", this.cross);
                img.setAttribute("id", i);
                img.onclick = function () {vm.delTask(this.id)};
                tabCell.appendChild(img);
                var input = document.createElement("input");
                input.setAttribute("type", "checkbox");
                input.setAttribute("class", "checkbox");
                input.setAttribute("id", i);
                if (this.done[id][i] == true) {
                    input.checked = true;
                    tabCell.setAttribute("class", "completed");
                } else
                    tabCell.setAttribute("class", "cellTask");
                input.onclick = function () {vm.addCheck(this.id, this.checked)};
                tabCell.appendChild(input);
                tr.appendChild(tabCell);
            }

            var divContainer = document.getElementById("showTask");
            divContainer.innerHTML = "";
            divContainer.appendChild(table);
            if (id != 0)
                this.buttonTask = true;
        },

        addList: function() {
            if (this.newListName != '') {
                this.list.push({name: this.newListName, task: []});
                this.newListName = '';
                this.done.push([]);
                this.createList();
                this.createTask(this.list.length - 1);
           }
        },

        addTask: function(id) {
            var table = document.getElementsByClassName("tableTask");

            if (this.newTaskName != '') {
                this.list[table[0].id].task.push(this.newTaskName);
                this.newTaskName = '';
                this.done[table[0].id].push(false);
                this.createTask(table[0].id);
           }
        },

        delTask: function(idTask) {
            var table = document.getElementsByClassName("tableTask");

            this.list[table[0].id].task.splice(idTask, 1);
            this.done[table[0].id].splice(idTask, 1);
            this.createTask(table[0].id);
        },

        addCheck: function(id, checked) {
            var table = document.getElementsByClassName("tableTask");

            if (checked)
                var tab = document.getElementsByClassName("cellTask");
            else
                var tab = document.getElementsByClassName("completed");

            for (var i = 0; i < tab.length; i++) {
                if (tab[i].id == id) {
                    if (checked) {
                        tab[i].setAttribute("class", "completed");
                        this.done[table[0].id][id] = true;
                    } else {
                        tab[i].setAttribute("class", "cellTask");
                        this.done[table[0].id][id] = false;
                    }
                }
            }
        },

        showDone: function() {
            console.log(this.done);
        }
    }
})
