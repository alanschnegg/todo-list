<!DOCTYPE html>

<html>

  <head>
    <meta charset="UTF-8">
    <link href="{{asset('css/todolist.css')}}" rel="stylesheet"/>
  </head>
  <body>
    <div class="main" id="app">
      <div class="header">
        <h1 class="title">MyTodoList</h1>
      </div> <! --header-- >

      <div class="todolist">
        <div class="list">
          <p id="showList"></p>
          <input class="text" v-model="newListName" placeholder="New list"></input>
          <button @click="addList">Add List</button>
        </div> <! --list-- >

        <div class="task">
          <p id="showTask"></p>
          <input class="text" v-model="newTaskName" placeholder="New Task"  v-if="buttonTask"></input>
          <button @click="addTask" class="buttonTask" v-if="buttonTask">Add Task</button>
        </div> <! --task-- >
      </div> <! --todolist-- >

    </div> <! --main-- >

      <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
      <script src="{{asset('js/todolist.js')}}"></script>

  </body>
</html>
