<html>

  <head>
    <meta charset="UTF-8">
    <link href="{{asset('css/index.css')}}" rel="stylesheet"/>
  </head>
  <body>
    <div class="main" id="app">
      <div class="header">
        <h1 class="title">MyTodoList</h1>
      </div> <! --header-- >


      <div class="connexion" v-if="connection">
        <form action="/todolist" method="get">
          <label for="email">Email: </label>
          <input type="email" id="email" required placeholder="your.name@email.com" v-model="email" name="email"/>
          <label for="password">Mot de passe: </label>
          <input :type="passwordFieldType" id="password" required placeholder="Mot de passe" v-model="password" name="password"/>
          <img :src="eyes" @click="showPassword">

          <a @click="switch_inscription">Pas encore inscrit ?</a>

          <button>Connexion</button>
        </form>
      </div> <! --connexion-- >


      <div class="inscription" v-if="inscription">
        <form action="/inscription" method="get">
          <label for="firstname">Prénom: </label>
          <input type="text" id="firstname" placeholder="Prénom" required v-model="firstname" name="firstname">

          <label for="lastname">Nom: </label>
          <input type="text" id="lastname" placeholder="Nom" required v-model="lastname" name="lastname">

          <label for="email">Email: </label>
          <input type="email" id="email" value="" required placeholder="your.name@email.com" v-model="email" name="email"/>

          <label for="password">Mot de passe: </label>
          <input :type="passwordFieldType" placeholder="Mot de passe" required v-model="password" name="password">
          <img :src="eyes" @click="showPassword">

          <a @click="switch_connexion">Déjà inscrit ?</a>

          <button>Inscription</button>
        </form>
      </div> <! --inscription-- >

    </div> <! --main-->

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="{{asset('js/index.js')}}"></script>

  </body>

</html>
